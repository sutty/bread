A feed retriever and parser that sends you e-mails, so you don't need
an specialized feed reader that you'll never open or keep up to.

It's tested with ~170 feeds from different sources.

* Sends e-mails

* Generates a single e-mail digest for feeds without content

* Uses caching, so minimal work and data transfer is required

* Uses List-ID so you can archive them, for instance with `notmuch` and
  `afew`.

* Sends replies when an article is updated

## Installation

```bash
git clone https://0xacab.org/sutty/bread.git
cd bread
bundle install
```

## Configuration

Copy `config.yml.example` to `config.yml` and edit.  `delivery_method`
accepts [mail](https://rubygems.org/gems/mail) options.

## Running

```bash
cd bread/
bundle exec bin/bread
```

## TODO

* Distribute as gem
* Store config and database at `~`
